# Správa uživatelů

## Build
```
# Instalace závislostí:
$ cd backend && composer install
$ cd ui && npm install

$ docker-compose build
```

## Spuštění
Vytvoříme adresář, do kterého je namapovaná cache ```Symfony``` a úložiště ```MySQL```.
```
$ mkdir -p ./.docker/var/{cache,log}
$ chmod -R 777 .docker/
```

Spustíme kontejnery (backend, ui, nginx, DB):
```
$ docker-compose up
```

Spuštění migrací:
```
docker exec mu-backend ./bin/console doctrine:migrations:migrate --no-interaction
```

-----

Aplikace je nyní dostupná na portu ```80```:
```
UI: 127.0.0.1
API: 127.0.0.1/api
```

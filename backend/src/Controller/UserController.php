<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Validation;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /** @var UserRepository */
    private $userRepository;

    /** @var UserPasswordEncoderInterface */
    private $encoder;

    public function __construct(UserRepository $userRepository, UserPasswordEncoderInterface $encoder)
    {
        $this->userRepository = $userRepository;
        $this->encoder = $encoder;
    }

    /**
     * Users listing
     * @Route(methods={"GET", "OPTIONS"})
     */
    public function index(): Response
    {
        $users = $this->userRepository->findAll();
        $data = [];

        foreach ($users as $user) {
            $data[] = [
                'id' => $user->getId(),
                'name' => $user->getName(),
                'email' => $user->getEmail(),
                'is_admin' => $user->isAdmin(),
            ];
        }

        return $this->json($data);
    }

    /**
     * Get user by ID
     * @Route("/{id}", methods={"GET", "OPTIONS"})
     */
    public function show(Request $request, int $id): Response
    {
        //find user by id
        $user = $this->userRepository->find($id);

        if (!$user) {
            return $this->json(["error" => "Unknown user"], 404);
        }

        return $this->json([
            'id' => $id,
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'is_admin' => $user->isAdmin(),
        ]);
    }

    /**
     * Get user by ID
     * @Route("/{id}", methods={"PUT", "OPTIONS"})
     * @param Request $request
     * @param int $id
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function store(Request $request, int $id, EntityManagerInterface $em): Response
    {
        //find user by id
        $user = $this->userRepository->find($id);

        if (!$user) {
            return $this->json(["error" => "Unknown user"], 404);
        }

        $validator = Validation::createValidator();

        $constraint = new Collection([
            'name' => new NotBlank(),
            'email' => new Email(),
            'is_admin' => new Type("bool"),
            'password' => new Optional(
                new Length(["min" => 6])
            ),
        ]);

        $data = json_decode($request->getContent(), true);

        //remove ID
        unset($data['id']);

        $violations = $validator->validate(
            $data,
            $constraint
        );

        //validate input data
        if ($violations->count() !== 0) {
            return $this->json(["error" => $violations->count()], 400);
        }

        //update
        $user->setName($data['name']);
        $user->setEmail($data['email']);
        $user->setIsAdmin($data['is_admin']);

        //only if user wnats to change a password
        if(isset($data['password'])) {
            $user->setPassword(
                $this->encoder->encodePassword(
                    $user,
                    $data['password']
                )
            );
        }

        try {
            $em->flush();
        } catch (UniqueConstraintViolationException $e) {
            return $this->json(["error" => "Email is not unique"], 400);
        }

        return $this->json([
            'id' => $user->getId(),
            'name' => $user->getName(),
            'email' => $user->getEmail(),
        ]);
    }

    /**
     * Create new user
     * @Route(methods={"POST", "OPTIONS"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function create(Request $request, EntityManagerInterface $em): Response
    {
        $validator = Validation::createValidator();

        $constraint = new Collection([
            'name' => new NotBlank(),
            'email' => new Email(),
            'is_admin' => new Type("bool"),
            'password' => new Length(["min" => 6]),
        ]);

        $data = json_decode($request->getContent(), true);
        $violations = $validator->validate(
            $data,
            $constraint
        );

        //validate input data
        if ($violations->count() !== 0) {
            return $this->json(["error" => "Invalid input data"], 400);
        }

        $user = new User();
        $user->setName($data['name']);
        $user->setEmail($data['email']);
        $user->setIsAdmin($data['is_admin']);
        $user->setPassword(
            $this->encoder->encodePassword(
                $user,
                $data['password']
            )
        );

        $em->persist($user);

        //persist user
        try {
            $em->flush();
        } catch (UniqueConstraintViolationException $e) {
            return $this->json(["error" => "Email is not unique"], 400);
        }

        return $this->json([
            'id' => $user->getId(),
            'name' => $user->getName(),
            'email' => $user->getEmail(),
        ]);
    }

    /**
     * Delete user by ID
     * @Route("/{id}", methods={"DELETE", "OPTIONS"})
     * @param Request $request
     * @param int $id
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete(Request $request, int $id, EntityManagerInterface $em): Response
    {
        //find user by id
        $user = $this->userRepository->find($id);

        if (!$user) {
            return $this->json(["error" => "Unknown user"], 404);
        }

        $em->remove($user);
        $em->flush();

        return $this->json([]);
    }
}
